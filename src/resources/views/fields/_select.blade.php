@php
        $fieldName = isset($field['multiple']) ? $field['name'].'[]' : $field['name'];
        // $selected = array_get( $field, 'value', config('app_settings.options.value'));
        // dd($selected);
@endphp

<select name="{{ $fieldName }}"
class="{{ array_get( $field, 'class', config('app_settings.input_class', 'form-control')) }}"
@if(isset($field['multi'])) multiple @endif
@if( $styleAttr = array_get($field, 'style')) style="{{ $styleAttr }}" @endif
id="{{ $field['name'] }}">
@foreach(array_get($field, 'options') as $val => $label)

@php
    $selected = array_get( $field, 'value', config('app_settings.options.value'));
@endphp

        <option
            value="{{ $val }}"
            @if( $selected == $val) selected
            {{-- @elseif( old($field['name'], \setting($field['name'])) == $val ) selected
            @elseif( $selected == $label) selected --}}
            @endif
            >
                {{ $label }}
        </option>
    @endforeach
</select>
