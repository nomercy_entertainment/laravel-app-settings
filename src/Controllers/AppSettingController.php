<?php

namespace NoMercy\AppSettings\Controllers;

use Illuminate\Routing\Controller;
use NoMercy\AppSettings\SavesSettings;

class AppSettingController extends Controller
{
    use SavesSettings;
}
