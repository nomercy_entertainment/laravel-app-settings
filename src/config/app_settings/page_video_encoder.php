<?php

function forEachBetween($min, $max, $default = null, $names = null, $extra = null)
{
    $options = array();

    if (isset($extra)) {
        $options = array_merge($extra, $options);
    }

    for ($x = $max; $x >= $min; $x--) {
        if(isset($default) && $x == array_key_first($default)){

        }
        else{
            $name = null;

            if(isset($names[$x])){
                $name = " " . $names[$x];
            }

            $option = [
                ' ' . $x => $x . $name,
            ];
            $options = array_merge($option, $options);
        }
    }

    if (isset($default)) {
        $options = array_merge($default, $options);
    }

    return $options;
}

$array = [
    'main' => [
        'title' => 'Main Settings',
        // 'descriptions' => 'Main ffmpeg settings.', // (optional)
        'icon' => 'fa fa-cog', // (optional)
        'inputs' => [
            [
                'type' => 'select',
                'name' => '-threads',
                'label' => 'Threads',
                'class' => 'form-control',
                'style' => 'color:black',
                'data_type' => 'int',
                'value' => '0',
                'hint' => 'Threads used.',
                'options' => forEachBetween(0, 16,
                    null,
                    [
                        '0' => 'Default',
                        '4' => 'Normal',
                        '8' => 'High',
                        '16' => 'Super High',
                    ],
                    [
                        ' 32' => '32 Not reccomended by ffmpeg'
                    ]
                ),
            ],
            [
                'type' => 'select',
                'name' => '-preset',
                'label' => 'Encoding speed.',
                'class' => 'form-control',
                'style' => 'color:black',
                'value' => 'veryslow',
                'hint' => 'Ultra fast requires more storage than Very slow',
                'options' => [
                    'medium' => 'Medium (Default)',
                    'veryslow' => 'Very slow',
                    'slower' => 'Slower',
                    'slow' => 'Slow',
                    'fast' => 'Fast',
                    'faster' => 'Faster',
                    'veryfast' => 'Very fast',
                    'superfast' => 'Super fast',
                    'ultrafast' => 'Ultra fast',
                ],
            ],
        ],
    ],
    'video' => [
        'title' => 'Video Encode Settings',
        // 'descriptions' => 'Video quality settings.', // (optional)
        'icon' => 'fa fa-file-video-o', // (optional)
        'inputs' => [
            [
                'name' => '-crf',
                'type' => 'select',
                'label' => 'CRF',
                'class' => 'form-control',
                'style' => 'color:black',
                'data_type' => 'int',
                'hint' => 'Overall quality.',
                'options' => forEachBetween(0,51,
                    [
                        ' 23' => '23 (Default)',
                    ],
                    [
                        0  => '(Lossless)',
                        15 => '(High)',
                        20 => '(Higher)',
                        23 => '(Default)',
                        26 => '(Lower)',
                        30 => '(Low)',
                        51 => '(Worst quality)',
                    ]
                ),
            ],
            [
                'name' => '-b:v',
                'type' => 'number',
                'label' => 'Bitrate',
                'class' => 'form-control',
                'style' => 'color:black',
                'value' => null,
                'placeholder' => 'Can be empty',
                'hint' => 'Average bitrate'
            ],
            [
                'name' => '-bufsize',
                'type' => 'number',
                'label' => 'Buffer size',
                'class' => 'form-control',
                'style' => 'color:black',
                'value' => null,
                'placeholder' => 'Can be empty',
                'hint' => 'Buffer size'
            ],
            [
                'name' => '-maxrate',
                'type' => 'number',
                'label' => 'Max bitrate',
                'class' => 'form-control',
                'style' => 'color:black',
                'value' => null,
                'placeholder' => 'Can be empty',
                'hint' => 'Maximum bitrate'
            ],
            [
                'type' => 'select',
                'name' => '-tune',
                'label' => 'Tune',
                'class' => 'form-control',
                'style' => 'color:black',
                'hint' => '.',
                'options' => [
                    'null' => 'None (Default)',
                    'animation' => 'Animation',
                    'film' => 'Film',
                    'fastdecode' => 'Fastdecode',
                    'grain' => 'Grain',
                ],
            ],
        ],
    ],
    'audio' => [
        'title' => 'Audio Encode Settings',
        // 'descriptions' => 'Audio quality settings.', // (optional)
        'icon' => 'fa fa-file-audio-o', // (optional)
        'inputs' => [
            [
                'name' => '-b:a',
                'type' => 'number',
                'label' => 'Bitrate',
                'class' => 'form-control',
                'style' => 'color:black',
                'data_type' => 'int',
                'value' => null,
                'placeholder' => 'Can be empty',
                'hint' => 'Audio bitrate'
            ],
            [
                'name' => '-ac',
                'type' => 'number',
                'label' => 'Channels',
                'class' => 'form-control',
                'style' => 'color:black',
                'data_type' => 'int',
                'value' => 6,
                'hint' => 'Audio channels (2=Stereo, 3=3.1, 6=5.1, 8=7.1)'
            ],
        ],
    ],
    'hls' => [
        'title' => 'Multi Audio Settings',
        // 'descriptions' => 'Audio quality settings.', // (optional)
        'icon' => 'fa fa-audio-description', // (optional)
        'inputs' => [
            [
                'name' => '-hls_time',
                'type' => 'number',
                'label' => 'HLS segment time',
                'class' => 'form-control',
                'style' => 'color:black',
                'rules' => 'required|min:1|max:15',
                'value' => 15,
                'data_type' => 'int',
                'hint' => 'Set the target duration for the individual .ts files.'
            ],
        ],
    ],
];

// dd($array['main']['inputs'][0]['options']);
return $array;
