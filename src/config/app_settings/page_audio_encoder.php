<?php
return [
    'title' => 'Encode Settings',
    'descriptions' => 'Application video encode settings.', // (optional)
    'icon' => 'fa fa-cog', // (optional)
    'inputs' => [
        [
            'name' => '-threads',
            'type' => 'number',
            'label' => 'CPU threads',
            'class' => 'form-control',
            'style' => 'color:black',
            'rules' => 'required|min:0|max:32',
            'value' => 0,
            'hint' => 'Set the amount of threads used by ffmpeg'
        ],
        [
            'type' => 'select',
            'name' => '-preset',
            'label' => 'Video encoding speed.',
            'class' => 'form-control',
            'style' => 'color:black',
            'value' => 'veryslow',
            'hint' => 'Ultra fast requires more storage than Very slow',
            'options' => [
                'ultrafast' => 'Ultra fast',
                'superfast' => 'Super fast',
                'veryfast' => 'Very fast',
                'faster' => 'Faster',
                'fast' => 'Fast',
                'medium' => 'Medium – default preset',
                'slow' => 'Slow',
                'slower' => 'Slower',
                'veryslow' => 'Very slow',
            ],
        ],
        [
            'name' => '-crf',
            'type' => 'number',
            'label' => 'Video CRF value',
            'min' => 0,
            'max' => 51,
            'rules' => 'min:0|max:51',
            'class' => 'form-control',
            'style' => 'color:black',
            'value' => null,
            'hint' => 'Default: 23.'
        ],
        [
            'name' => '-b:v',
            'type' => 'number',
            'label' => 'Video bitrate',
            'class' => 'form-control',
            'style' => 'color:black',
            'value' => null,
            'hint' => '.'
        ],
        [
            'name' => '-bufsize',
            'type' => 'number',
            'label' => 'Video buffer size',
            'class' => 'form-control',
            'style' => 'color:black',
            'value' => null,
            'hint' => '.'
        ],
        [
            'name' => '-maxrate',
            'type' => 'number',
            'label' => 'Video max bitrate',
            'class' => 'form-control',
            'style' => 'color:black',
            'value' => null,
            'hint' => '.'
        ],
        [
            'type' => 'select',
            'name' => '-tune',
            'label' => 'Video Tune',
            'class' => 'form-control',
            'style' => 'color:black',
            'hint' => '.',
            'options' => [
                'null' => '',
                'animation' => 'Animation',
                'film' => 'Film',
                'fastdecode' => 'Fastdecode',
                'grain' => 'Grain',
            ],
        ],
        [
            'name' => '-hls_time',
            'type' => 'number',
            'label' => 'Video HLS segment time',
            'class' => 'form-control',
            'style' => 'color:black',
            'rules' => 'required|min:1|max:15',
            'value' => 15,
            'hint' => 'Set the target duration for the individual .ts files. (Multi audio only)'
        ],
    ],
];
