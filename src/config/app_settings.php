<?php

return [

    // All the sections for the settings page
    'sections' => [
        'branding' => require(__DIR__ . '/app_settings/page_branding.php'),
        'email' => require(__DIR__ . '/app_settings/page_email.php'),
        'encoder' => require(__DIR__ . '/app_settings/page_video_encoder.php'),

    ],

    // Setting page url, will be used for get and post request
    'url' => '/settings',

    // Any middleware you want to run on above route
    // 'middleware' => ['auth:web'],
    'middleware' => [],

    // View settings
    'setting_page_view' => 'app_settings::settings_page',
    'flash_partial' => 'app_settings::_flash',

    // Setting section class setting
    'section_heading_class' => 'card-header',
    'section_body_class' => 'card-body bg-dark text-light',
    'section_class' => 'card mb-4 px-0 col-12 float-left ',

    // Input wrapper and group class setting
    'input_wrapper_class' => 'form-group col-lg-3 float-left ',
    'input_class' => 'form-control',
    'input_error_class' => 'has-error',
    'input_invalid_class' => 'is-invalid',
    'input_hint_class' => 'form-text text-light',
    'input_error_feedback_class' => 'text-danger',

    // Submit button
    'submit_btn_text' => 'Save Settings',
    'submit_success_message' => 'Settings has been saved.',

    // Remove any setting which declaration removed later from sections
    'remove_abandoned_settings' => false,

    // Controller to show and handle save setting
    'controller' => '\NoMercy\AppSettings\Controllers\AppSettingController',

    // settings group
    'setting_group' => function () {
        // return 'user_'.auth()->id();
        return 'ffmpeg';
    }
];
